namespace BlogSitesi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Comment")]
    public partial class Comment
    {
        public int CommentID { get; set; }

        [StringLength(300)]
        public string CommentContent { get; set; }

        public DateTime? Date { get; set; }

        public int? ArticleID { get; set; }

        public int? UserID { get; set; }

        public virtual Article Article { get; set; }

        public virtual User User { get; set; }
    }
}
