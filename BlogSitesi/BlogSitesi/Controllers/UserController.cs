﻿using BlogSitesi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogSitesi.Controllers
{
    public class UserController : Controller
    {
        blogDB db = new blogDB();
        // GET: User
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(User user)
        {
            var login = db.Users.Where(m => m.Nickname == user.Nickname).SingleOrDefault();
            if (login.Nickname == user.Nickname && login.Password == user.Password)
            {
                Session["userid"] = user.UserID;
                Session["username"] = user.Nickname;
                Session["roleid"] = login.RoleID;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult Logout()
        {
            Session["userid"] = null;
            Session.Abandon();

            return RedirectToAction("Index", "Home");
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                user.RoleID = 2;
                db.Users.Add(user);
                db.SaveChanges();
                Session["userid"] = user.UserID;
                Session["username"] = user.Nickname;
                return RedirectToAction("Index", "Home");
            }
            return View(user);
        }
    }
}