﻿using BlogSitesi.Models;//Veri tabanı bağlantısı için gereken 1. şey
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogSitesi.Controllers
{
    public class HomeController : Controller
    {
        blogDB db = new blogDB();//VT bağlantısı için gereken 2. şey
        // GET: Home
        public ActionResult Index()
        {
            var article = db.Articles.OrderByDescending(m => m.ArticleID).ToList();
            return View(article);
        }
        public ActionResult DetailedArticle(int id)
        {
            var article = db.Articles.Where(m => m.ArticleID == id).SingleOrDefault();
            if (article == null)
            {
                return HttpNotFound();
            }
            return View(article);
        }
        public ActionResult Hakkimizda()
        {
            return View();
        }
        public ActionResult Iletisim()
        {
            return View();
        }
        public ActionResult CategoryPartial()
        {
            return View(db.Categories.ToList());
        }
    }
}