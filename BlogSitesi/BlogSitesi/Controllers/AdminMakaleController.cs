﻿using BlogSitesi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogSitesi.Controllers
{
    public class AdminMakaleController : Controller
    {
        blogDB db = new blogDB();
        // GET: AdminMakale
        public ActionResult Index()
        {
            var articles = db.Articles.ToList();
            return View(articles);
        }

        // GET: AdminMakale/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: AdminMakale/Create
        public ActionResult Create()
        {
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "Name");
            return View();
        }

        // POST: AdminMakale/Create
        [HttpPost]
        public ActionResult Create(Article article)
        {
            try
            {
                // TODO: Add insert logic here
                db.Articles.Add(article);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: AdminMakale/Edit/5
        public ActionResult Edit(int id)
        {
            var article = db.Articles.Where(m => m.ArticleID == id).SingleOrDefault();
            if (article == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "Name", article.CategoryID);
            return View(article);
        }

        // POST: AdminMakale/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Article article)
        {
            try
            {
                // TODO: Add update logic here
                var articles = db.Articles.Where(m => m.ArticleID == id).SingleOrDefault();

                articles.Title = article.Title;
                articles.ArticleContent = article.ArticleContent;
                articles.CategoryID = article.CategoryID;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "Name", article.CategoryID);
                return View(article);
            }
        }

        // GET: AdminMakale/Delete/5
        public ActionResult Delete(int id)
        {
            var article = db.Articles.Where(m => m.ArticleID == id).SingleOrDefault();
            if (article == null)
            {
                return HttpNotFound();
            }
            return View(article);
        }

        // POST: AdminMakale/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                var articles = db.Articles.Where(m => m.ArticleID == id).SingleOrDefault();
                if (articles == null)
                {
                    return HttpNotFound();
                }
                foreach(var i in articles.Comments.ToList())
                {
                    db.Comments.Remove(i);
                }
                db.Articles.Remove(articles);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
